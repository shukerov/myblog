---
title: A Middleman Blog Post Test
date: 2017/12/19
tags: blogging, middleman, hello, world
---
So I guess I can start writing, and doing some blogging already. The first blog would be me figuring
out how to deal with middleman and configure it to do syntax highlighting, as well as use partials to format my web page.
Here is some more words to make the summary nice and cool.

Below we have some examples of syntax highlighting for python and ruby:

Python:

```python
s = "python syntax highlighting test"
print(s)
print("Test was succesful!")
```

Ruby:

```ruby
def my_cool_method(message)
  puts message
end
```

This is an image test:
![Test Image](test.png)

Looks like things seem to work. Note to self: delete this blogpost after you write a few more good ones.
